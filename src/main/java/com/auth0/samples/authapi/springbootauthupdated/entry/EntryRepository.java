package com.auth0.samples.authapi.springbootauthupdated.entry;


import org.springframework.data.jpa.repository.JpaRepository;

public interface EntryRepository extends JpaRepository<Entry, Long> {
}
