package com.auth0.samples.authapi.springbootauthupdated.entry;



import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.core.Response;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/entries")
public class EntryController {
    private EntryService entryService;

    public EntryController(EntryService entryService) {
        this.entryService = entryService;
    }
    
    /**
     * getAllEntries
     * Gibt alle Entries aus
     * @return
     */
    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    public List<Entry> getAllEntries() {
        return entryService.findAllEntries();
    }
    /**
     * getAllEntriesResponse
     * gibt alle responses aus
     * @return
     */
    @GetMapping("/allresponses")
//    @Produces({ "application/json" })
    public Response getAllEntriesResponse() {
		return Response.status(200)
				.entity(entryService.findAllEntries())
				.build();
	}
    
    /**
     * deleteEmployee
     * l�scht entry
     * @param id
     */
    @DeleteMapping("/remove/{id}")
    void deleteEmployee(@PathVariable Long id) {
      entryService.removeEntryById(id);
    }
    
    /**
     * newEntry
     * erstellt neuen entry
     * @param newEntry
     * @return
     */
    @PostMapping("/do")
    Entry newEntry(@RequestBody Entry newEntry) {
      return entryService.createEntry(newEntry);
    }
    
    /**
     * replaceEntry
     * Ver�ndert einen Entry
     * @param newEntry
     * @param id
     * @return
     */
    @PutMapping("/change/{id}")
    Entry replaceEntry(@RequestBody Entry newEntry, @PathVariable Long id) {

 

      return entryService.changeEntryById(newEntry, id);
    }
    
    

}
