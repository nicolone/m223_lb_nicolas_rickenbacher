package com.auth0.samples.authapi.springbootauthupdated.shoppinglist;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 
 * @author Nicolas Rickenbacher
 * @date 10.07.2020
 *
 */

public interface ShoppingListRepository extends JpaRepository<ShoppingList, Long> {

}
