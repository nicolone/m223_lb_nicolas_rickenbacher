package com.auth0.samples.authapi.springbootauthupdated.shoppinglist;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/**
 * 
 * @author Nicolas Rickenbacher
 * @date 10.07.2020
 *
 */
@Entity
public class ShoppingList {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private int anzahl;
    private String description;

    protected ShoppingList() { }

    public ShoppingList(String description, int anzahl) {
        this.description = description;
        this.anzahl = anzahl;
    }

    public int getAnzahl() {
		return anzahl;
	}

	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}

	public long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}