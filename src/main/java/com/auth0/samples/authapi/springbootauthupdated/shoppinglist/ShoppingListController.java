package com.auth0.samples.authapi.springbootauthupdated.shoppinglist;

import java.util.List;

import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @author Nicolas Rickenbacher
 * @date 10.07.2020
 *
 */
@RestController
@RequestMapping("/shopping")
public class ShoppingListController {
	 private ShoppingListRepository shoppingRepository;

	    public ShoppingListController(ShoppingListRepository shoppingListRepository) {
	        this.shoppingRepository = shoppingListRepository;
	    }
	    
	    /**
	     * addEintrag
	     * neuer Eintrag wird in die ShoppingList hinzugef�gt
	     * @param shoppinglist
	     */
	    @PostMapping
	    public void addEintrag(@RequestBody ShoppingList shoppinglist) {
	    	shoppingRepository.save(shoppinglist);
	    }
	    
	    /**
	     * getEintrag
	     * eingetragene Eintr�ge werden angezeigt
	     * @return
	     */
	    @GetMapping 
	    public List<ShoppingList> getEintrag() {
	        return shoppingRepository.findAll();
	    }

	    /**
	     * editEintrag
	     * Eintrag wird ver�ndert
	     * @param id
	     * @param shoppinglist
	     */
	    @PutMapping("/{id}")
	    public void editEintrag(@PathVariable long id, @RequestBody ShoppingList shoppinglist) {
	    	ShoppingList existingEintrag = shoppingRepository.findById(id).get();
	        Assert.notNull(existingEintrag, "ShoppingList not found");
	        existingEintrag.setDescription(shoppinglist.getDescription());
	        existingEintrag.setAnzahl(shoppinglist.getAnzahl());
	        shoppingRepository.save(existingEintrag);
	    }
	    
	    /**
	     * deleteEintrag
	     * Eintrag wird gel�scht
	     * @param id
	     */
	    @DeleteMapping("/{id}")
	    public void deleteEintrag(@PathVariable long id) {
	    	ShoppingList listToDel = shoppingRepository.findById(id).get();
	        shoppingRepository.delete(listToDel);
	    }
	}